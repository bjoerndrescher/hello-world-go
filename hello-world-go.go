package main

import (
    "log"
    "net/http"
    "github.com/gorilla/mux"
    "hello-world-go/models"
)

func main() {
  log.Printf("Starting version 1...")

  router := mux.NewRouter()
  router.HandleFunc("/resource", func(w http.ResponseWriter, r *http.Request) { models.GetResource(w,r)}).Methods("GET")
  router.HandleFunc("/resource/{key:[0-9]+}", func(w http.ResponseWriter, r *http.Request) { models.GetResourceByKey(w,r)}).Methods("GET")

  log.Fatal(http.ListenAndServe(":8091", router))
}
