package models

import (
  "log"
  "net/http"
  "github.com/gorilla/mux"
  "strconv"
  "encoding/json"
)

func GetResource(w http.ResponseWriter, r *http.Request) {
  log.Printf("GET request received")

  var resultSet = []Resource {
    {1, "Test"},
    {2, "Test2"}}

  w.Header().Set("Content-Type", "encoding/json; charset=UTF-8")
  json.NewEncoder(w).Encode(resultSet)
}


func GetResourceByKey(w http.ResponseWriter, r *http.Request) {

  vars := mux.Vars(r)
  key := vars["key"]

  var result = Resource {0,"null"}
  var resultSet = []Resource {
    {1, "Test"},
    {2, "Test2"}}

  log.Printf(key)

  p, err := strconv.Atoi(key)
  if err != nil {
    log.Println("Param could not be converted to Int")
  }

  for i := range resultSet {
    log.Printf("Result %d, %d", i, resultSet[i])
    if resultSet[i].ID == p {
      log.Printf("Hit")
      result = resultSet[i]
    }
  }

  if result.Name != "null" {
    json.NewEncoder(w).Encode(result)
  } else {
    w.Header().Set("Content-Type", "text/plain; charset=UTF-8")
    w.WriteHeader(404)
  }
}

type Resource struct {
  ID int
  Name string
}
